# ADC转换 - 读取电位器电压（ADC_Potentiometer）

通过ADC读取电位器的电压值

## 如何使用例程

下载程序，并连接USB虚拟串口线，即可看到效果

#### 硬件连接

- 使用配套TYPE-C数据线，将学习板连接到计算机

<img src="Doc/连接usb线.png" style="zoom:35%;" />

#### 程序效果

- 打开[波特律动 串口助手 (keysking.com)](https://serial.keysking.com/)在线串口调试助手，点击“选择串口”，选择USB Single Serial，即可收到ADC转换结果，如图所示

<img src="Doc/串口数据.png" style="zoom:56%;" />

- 程序会连续读取PA5的输入电压值，而PA5的电压值是由电位器VOL决定的，使用螺丝刀调节学习板上的电位器，即可观察到对应的数据变化。

  > 数据变化范围0-4095，对应着PA5的电压值0-3.3V

<img src="Doc/1.png" style="zoom:60%;" />


## 例程讲解

下面介绍了如何自己实现该例程的功能

### 1、工程配置

- **打开ADC1：**在Pinout&Configuration页面，将PA5设置为ADC1_IN5

- **配置ADC：**在Pinout&Configuration -> Analog -> ADC1 -> Configuration中
  - ADC_Settings -> Continuous Conversion Mode设为Enable，使ADC转换持续进行，不需要每次获取之前手动触发转换
  - ADC_Regular_ConversionMode -> Rank -> Sampling Time设为239.5 Cycles，最长采样时间，可以获得更稳定的转换结果
  
- **打开串口2外设：**Pinout&Configuration -> Connectivity -> USART2，将Mode选择为Asynchronous

- **启用float打印：**在cubeIDE菜单栏中，Project Properties -> C/C++ Build -> Settings -> Tool Settings -> MCU Settings，勾选Use float with printf ... -nano

  > 默认情况下，sprintf函数不能打印小数。因此我们需要配置一下编译器，使其能够打印小数

### 2、代码

#### (1) 初始化过程

```c
// 启动连续ADC转换
HAL_ADC_Start(&hadc1);
// 等待ADC稳定
HAL_Delay(500);
```

#### (2) 读取ADC结果

```c
// 获取ADC值
result = HAL_ADC_GetValue(&hadc1);
// 计算电压值：电压 = ADC结果 × 3.3V ÷ 4095
voltage = result * 3.3f / 4095;
// 将变量打印为字符串
sprintf(send_buf, "原始值: %d，电压值: %.3f V\r\n", result, voltage);
// 通过串口2发送
HAL_UART_Transmit(&huart2, (uint8_t*) send_buf, strlen(send_buf), 20);
```

## 故障排除

### 工程建立和配置问题

- **cube重新生成代码后，中文出现乱码：**这是cubeIDE的问题，我们在环境变量中添加一行配置即可解决（仅Windows下）

  - 点击开始菜单，输入“环境变量”搜索，进入系统属性设置

  <img src="Doc/搜索环境变量.png" style="zoom:80%;" />

  - 点击系统属性下方的“环境变量”，进入环境变量配置页面。如图，点击新建，添加一个环境变量并保存即可。

    变量名：JAVA_TOOL_OPTIONS

    变量值：-Dfile.encoding=UTF-8

  <img src="Doc/添加环境变量.png" style="zoom:58%;" />

- **找不到gpio.c文件：**工程目录中打开xxx.ioc配置文件，进入Project Manager-Code Generator，勾选Generate peripheral initialization as a pair of '.c/.h' files per peripheral，保存并重新生成代码，就有单独的gpio.c文件了

<img src="Doc/勾选生成独立初始化文件.png" style="zoom: 68%;" />

- **打开.ioc配置文件时弹出对话框”New STM32Cube firmware version available“：**因为您的软件版本比例程高，此时建议直接点击”Migrate“迁移到您的版本即可，以后再打开就不会出现了。

<img src="Doc/New version dialog.png" style="zoom: 80%;" />

### 程序下载问题

- **下载程序时，提示如图错误：Failed to start GDB server** 

<img src="Doc/错误提示.Png" style="zoom: 67%;" />

可能是由于上次下载程序时关闭了debug接口。请将Boot0、Boot1跳线帽切换到H，并按下RST键，再尝试下载。如果此时下载成功，将跳线帽切换回L，按下RST键后程序即正常运行。

**注意：为了避免下次出现该问题**，请在工程配置中，找到System Core -> SYS -> Debug，选择Serial Wire，重新生成代码。这样即可避免程序将debug接口关闭导致的下载失败。

<img src="Doc/选择debug模式.png" style="zoom:60%;" />

- **下载程序时，提示“需要升级ST-LINK固件”，如图所示**

<img src="Doc/需要升级STLINK固件.png" style="zoom:67%;" />

升级ST-LINK固件即可解决，请按如下步骤操作：

1、 顶部工具栏 Help -> ST-LINK更新，打开固件升级工具。

<img src="Doc/STLINK更新.png" style="zoom:60%;" />

2、重新插拔一下ST-LINK使其进入DFU模式

3、点击“Refresh Device List”，然后点击2次“Open in update mode”，直到Version显示出来。（如果Version仍然是Unknown，再点击一次“Open in update mode”）

<img src="Doc/open_in_dfu.png" style="zoom:67%;" />

4、点击“Upgrade”，进度条走完后，重新插拔ST-LINK，即可正常下载。

