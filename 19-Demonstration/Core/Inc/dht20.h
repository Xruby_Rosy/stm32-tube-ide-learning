#ifndef __DHT20_H__
#define __DHT20_H__

#include "main.h"
#include "i2c.h"

// DHT20初始化寄存器
void DHT20_Reset_REG(uint8_t addr);

// 初始化DHT20
void DHT20_Init(void);

// DHT20读温湿度
void DHT20_Get_T_H(float *Temperature, float *Humidity);

#endif
