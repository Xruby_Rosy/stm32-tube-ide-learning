#include "ws2812.h"

uint16_t ws2812_data[RST_PERIOD_NUM + WS2812_NUM * 24] = { 0 };

void ws2812_data_set(uint8_t led_id, uint8_t r, uint8_t g, uint8_t b) {

	uint16_t *p = ws2812_data + RST_PERIOD_NUM + led_id * 24;
	for (uint8_t i = 0; i < 8; i++) {
		p[i] = (r << i) & (0x80) ? CODE_ONE_DUTY : CODE_ZERO_DUTY;
		p[i + 8] = (g << i) & (0x80) ? CODE_ONE_DUTY : CODE_ZERO_DUTY;
		p[i + 16] = (b << i) & (0x80) ? CODE_ONE_DUTY : CODE_ZERO_DUTY;
	}
}

void ws2812_updata(void) {
	HAL_TIM_PWM_Start_DMA(&htim3, TIM_CHANNEL_1, ws2812_data,
			RST_PERIOD_NUM + WS2812_NUM * 24);
}

