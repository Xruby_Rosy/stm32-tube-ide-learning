#ifndef __WS2812_H__
#define __WS2812_H__

#include "main.h"
#include "gpio.h"
#include "tim.h"

#define CODE_ONE_DUTY	66
#define CODE_ZERO_DUTY	21
//#define CODE_ONE_DUTY	90
//#define CODE_ZERO_DUTY	32
#define RST_PERIOD_NUM	100
#define WS2812_NUM		10

extern uint16_t ws2812_data[RST_PERIOD_NUM + WS2812_NUM * 24];

void ws2812_data_set(uint8_t led_id, uint8_t r, uint8_t g, uint8_t b);

void ws2812_updata(void);

#endif
