/*
 * comm.c
 *
 *  Created on: Aug 31, 2023
 *      Author: lisheng.xiao
 */


#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "usart.h"

#include "comm.h"


/**************************************************************************
							相 关 变 量 说 明：
int arg = 0;----- 数组转换标志位：arg：0--cmd  1--argv1[16]  2--argv2[16]
int inde = 0;---- 数组元素标志位

char chr;-------- 用于读取串口数据数组的转换字符型数据

char cmd;-------- 用于储存相关字符型编号任务
char argv1[16];-- 用于储存第一段数字数据的数组
char argv2[16];-- 用于储存第二段数字数据的数组

int arg1;-------- 用于将argv1[16]储存的数字数据解析出来
int arg2;-------- 用于将argv2[16]储存的数字数据解析出来
**************************************************************************/
int arg = 0;
int inde = 0;

char chr;

char cmd;
char argv1[16];
char argv2[16];

int arg1;
int arg2;

/**************************************************************************
函数功能：重置相关变量，用于下次数据解析
入口参数：无
返回  值：无
说    明：将数据解析中的相关标志位、数据存储变量、数组数据等重新置位
**************************************************************************/
void resetCommand(void)
{
  cmd = NULL;
  memset(argv1, 0, sizeof(argv1));
  memset(argv2, 0, sizeof(argv2));
  arg1 = 0;
  arg2 = 0;
  arg = 0;
  inde = 0;
}

/**************************************************************************
函数功能：执行相关数据命令函数
入口参数：无
返回  值：无
说    明：数据解析完成后，将相关数据用于执行任务
**************************************************************************/
void runCommand(void)
{
	arg1 = atoi(argv1);
  	arg2 = atoi(argv2);

	UART2_printf_DMA("Number-1 is %d \r\n",arg1);
	HAL_Delay(10);
	UART2_printf_DMA("Number-2 is %d \r\n",arg2);
	HAL_Delay(10);

	switch (cmd)
	{
		case 'A':
			UART2_printf_DMA("cmd = A");
			break;
		case 'B':
			UART2_printf_DMA("cmd = B");
			break;
		case 'C':
			UART2_printf_DMA("cmd = C");
			break;
		case 'D':
			UART2_printf_DMA("cmd = D");
			break;
		case 'E':
			UART2_printf_DMA("cmd = E");
			break;
		case 'F':
			UART2_printf_DMA("cmd = F");
			break;
		case 'G':
			UART2_printf_DMA("cmd = G");
			break;
		case 'H':
			UART2_printf_DMA("cmd = H");
			break;
		default:
			UART2_printf_DMA("cmd = Other");
			break;
	}

}


/**************************************************************************
函数功能：收到上位机发来的数据后进行处理
入口参数：uint8_t *buff: 接收到的串口数组;	uint16_t Length：数组长度;
返回  值：无
说    明：搭配串口空闲中断进行数据解析；
		  串口2空闲中断处理函数：UsartReceive_IDLE(&huart2);
		  ---串口2空闲处理函数得到：相关串口数据数组：UsartType2.usartDMA_rxBuf；
								  相关串口数据数组长度：UsartType2.rx_len；
		  数据接收处理函数：DateProcess(UsartType2.usartDMA_rxBuf,UsartType2.rx_len);
**************************************************************************/
void DateProcess(uint8_t *buff, uint16_t Length)
{
	int i=0;

	for(i=0;i<Length;i++)
	{
		chr = buff[i];

		//使用回车命令（CR）中断解析
		if (chr == 13)
		{
			if (arg == 1) argv1[inde] = NULL;
			else if (arg == 2) argv2[inde] = NULL;
			break;
		}
		//使用空格（','）限定参数部分的数据命令内容
		else if (chr == ',')
		{
			//逐项设置相关数组的转换变量
			if (arg == 0) arg = 1;
			else if (arg == 1)
			{
				argv1[inde] = NULL;
				arg = 2;
				inde = 0;
			}
			continue;
		}
		else
		{
			if (arg == 0)
			{
				//第一个参数是单字母命令
				cmd = chr;
			}
			else if (arg == 1)
			{
				//后续的参数可以是一个以上的字符，并将相关命令数组解析出
				argv1[inde] = chr;
				inde++;
			}
			else if (arg == 2)
			{
				argv2[inde] = chr;
				inde++;
			}
		}
	}

	runCommand();
  resetCommand();
}
