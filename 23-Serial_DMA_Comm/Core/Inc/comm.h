/*
 * comm.h
 *
 *  Created on: Aug 31, 2023
 *      Author: lisheng.xiao
 */

#ifndef INC_COMM_H_
#define INC_COMM_H_
#include "main.h"
#include "gpio.h"

void resetCommand(void);
void runCommad(void);
void DateProcess(uint8_t *buff,uint16_t Length);


#endif /* INC_COMM_H_ */
