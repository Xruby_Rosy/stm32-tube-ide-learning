/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_BULE_Pin GPIO_PIN_6
#define LED_BULE_GPIO_Port GPIOA
#define LED_GREEN_Pin GPIO_PIN_7
#define LED_GREEN_GPIO_Port GPIOA
#define LED_RED_Pin GPIO_PIN_0
#define LED_RED_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define ON  GPIO_PIN_RESET
#define OFF GPIO_PIN_SET
#define LED1(a)	HAL_GPIO_WritePin(LED_BULE_GPIO_Port,LED_BULE_Pin,a)


#define LED2(a)	HAL_GPIO_WritePin(LED_GREEN_GPIO_Port,LED_GREEN_Pin,a)


#define LED3(a)	HAL_GPIO_WritePin(LED_RED_GPIO_Port,LED_RED_Pin,a)
#define	digitalHi(p,i)			{p->BSRR=i;}			  //����Ϊ�ߵ�ƽ
#define digitalLo(p,i)			{p->BSRR=(uint32_t)i << 16;}				//����͵�ƽ
#define digitalToggle(p,i)		{p->ODR ^=i;}			//�����ת״̬
#define LED1_TOGGLE		digitalToggle(LED_BULE_GPIO_Port,LED_BULE_Pin)
#define LED1_OFF		digitalHi(LED_BULE_GPIO_Port,LED_BULE_Pin)
#define LED1_ON			digitalLo(LED_BULE_GPIO_Port,LED_BULE_Pin)

#define LED2_TOGGLE		digitalToggle(LED_GREEN_GPIO_Port,LED_GREEN_Pin)
#define LED2_OFF		digitalHi(LED_GREEN_GPIO_Port,LED_GREEN_Pin)
#define LED2_ON			digitalLo(LED_GREEN_GPIO_Port,LED_GREEN_Pin)

#define LED3_TOGGLE		digitalToggle(LED_RED_GPIO_Port,LED_RED_Pin)
#define LED3_OFF		digitalHi(LED_RED_GPIO_Port,LED_RED_Pin)
#define LED3_ON			digitalLo(LED_RED_GPIO_Port,LED_RED_Pin)

//��
#define LED_RED  \
					LED1_ON;\
					LED2_OFF\
					LED3_OFF

//��
#define LED_GREEN		\
					LED1_OFF;\
					LED2_ON\
					LED3_OFF

//��
#define LED_BLUE	\
					LED1_OFF;\
					LED2_OFF\
					LED3_ON


//��(��+��)
#define LED_YELLOW	\
					LED1_ON;\
					LED2_ON\
					LED3_OFF
//��(��+��)
#define LED_PURPLE	\
					LED1_ON;\
					LED2_OFF\
					LED3_ON

//��(��+��)
#define LED_CYAN \
					LED1_OFF;\
					LED2_ON\
					LED3_ON

//��(��+��+��)
#define LED_WHITE	\
					LED1_ON;\
					LED2_ON\
					LED3_ON

//��(ȫ���ر�)
#define LED_RGBOFF	\
					LED1_OFF;\
					LED2_OFF\
					LED3_OFF


/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
